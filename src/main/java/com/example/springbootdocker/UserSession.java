package com.example.springbootdocker;

import java.util.Date;

import org.springframework.data.redis.core.RedisHash;

@RedisHash("Session")
public class UserSession {

    private String id;
    private String username;
    private Date loginTime;

    public UserSession(String id, String username, Date loginTime) {
        this.id = id;
        this.username = username;
        this.loginTime = loginTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    @Override
    public String toString() {
        return "UserSession{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", loginTime=" + loginTime + '\'' +
                '}';
    }
}
