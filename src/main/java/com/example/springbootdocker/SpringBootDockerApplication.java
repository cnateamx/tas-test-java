package com.example.springbootdocker;

import org.apache.catalina.Store;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Spliterator;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;

@SpringBootApplication
@RestController
@EnableRabbit
public class SpringBootDockerApplication {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private String firstname;
    private String lastname;

    @Autowired
    public void setDataSource(DataSource datasource) {
        this.jdbcTemplate = new JdbcTemplate(datasource);
    }

    @Autowired
    private UserSessionRepository userSessionRepository;

    @Autowired
    private QueueSender queueSender;

    @RequestMapping("/")
    public String home() {
        User user = jdbcTemplate.queryForObject("SELECT * FROM my_users where username=?",
                new Object[]{1},
                new UserMapper());
        firstname = user.getFirstname();
        lastname = user.getLastname();

        return "Hello " + firstname + " " + lastname + "!";
    }

    @RequestMapping("/redisSave")
    public String redisSave() {
        UserSession userSession = new UserSession("myUniqueID",
                "myUsername", new Date());

        UserSession saved = userSessionRepository.save(userSession);
        
        return "Saved to redis: " + saved.toString();
    }

    @RequestMapping("/redisLoad")
    public String redisLoad() {

        Optional<UserSession> userSessionOptional = userSessionRepository.findById("myUniqueID");
        
        if (userSessionOptional.isPresent()){
            UserSession currentSession = userSessionOptional.get();
            return "This is retrieved from redis: my username is " + currentSession.getUsername() + ", my log in time is : " + currentSession.getLoginTime();
        } else{
            return "Nothing to retrieve. :(";
        }
    }

    @RequestMapping("/sendMessage")
    public String rabbitSendTest() {
        queueSender.send("this test message is from java app");
        return "ok. have sent the message to the queue!!";

    }

    public static void main(String[] args) {
        SpringApplication.run(SpringBootDockerApplication.class, args);
    }

    /**
     * Nested static class to act as RowMapper for User object
     */
    private static class UserMapper implements RowMapper<User> {
        public User mapRow(ResultSet rs, int rowNum) throws SQLException {
            User user = new User();
            user.setUsername(rs.getLong("username"));
            user.setFirstname(rs.getString("firstname"));
            user.setLastname(rs.getString("lastname"));
            return user;
        }
    }
}
