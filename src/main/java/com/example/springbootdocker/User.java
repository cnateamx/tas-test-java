package com.example.springbootdocker;

public class User {
    protected long username;
    protected String firstname;
    protected String lastname;

    public User() {}

    public User(long username, String firstname, String lastname) {
        this.username = username;
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public long getUsername() {
        return username;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setUsername(long username) {
        this.username = username;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Override
    public String toString() {
        return String.format(
            "User[username=%d, firstname='%s', lastname='%s']",
            username, firstname, lastname);
    }
}
